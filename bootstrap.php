<?php
// bootstrap.php

session_start();
#session_destroy();

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use bw_cart\Core\Autoloader;

require_once 'vendor/autoload.php';
require_once 'config/config.php';

require_once 'src/Core/Autoloader.php';

/**
 * Doctrine Configuration
 */
$isDevMode = true;

$GLOBALS = [
    '_doctrine_config'      =>  Setup::createAnnotationMetadataConfiguration(array(__DIR__ . ENTITY_DIR), $isDevMode),
    '_doctrine_connection'  =>  [
        'driver'    =>  'pdo_mysql',
        'user'      =>  MYSQL_USER,
        'password'  =>  MYSQL_PASS,
        'dbname'    =>  MYSQL_NAME
    ]
];

$entityManager = EntityManager::create($GLOBALS['_doctrine_connection'], $GLOBALS['_doctrine_config']);

/**
 * Set PHP error reporting
 */
switch (APP_ENVIRONMENT) {
    case 'dev':
        error_reporting(E_ALL & ~E_NOTICE);
        break;
    case 'live':
        error_reporting(0);
        break;
}

/**
 * Autoload classes from namespace 'bw_cart'
 */
$autoloader = new Autoloader('bw_cart', 'src/');
$autoloader->register();
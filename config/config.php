<?php
// config.php

/**
 * MySQL Configuration
 */
define('MYSQL_HOST', 'localhost');
define('MYSQL_USER', 'root');
define('MYSQL_PASS', '');   // FILL IN YOUR PASSWORT HERE
define('MYSQL_NAME', 'cart');

/**
 * Directory Configuration
 */
define('ENTITY_DIR', DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR .'Models');
define('CONTROLLER_DIR', 'src/Controllers');
define('VIEW_DIR', 'src/Views');
define('CORE_DIR', 'src/Core');

define('DOMAIN_DIR', 'https://localhost/cart');


/**
 * Application Environment
 *
 * Set the App Environment to one of the following:
 *  - dev -> full PHP error reporting
 *  - live -> no PHP error reporting
 */
define('APP_ENVIRONMENT', 'dev');
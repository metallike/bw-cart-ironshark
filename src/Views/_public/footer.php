<?php
// src/Views/_public/footer.php

use bw_cart\Core\View;
?>

        <script src="<?php echo DOMAIN_DIR . '/' . VIEW_DIR;?>/js/vendor/jquery.js"></script>
        <script src="<?php echo DOMAIN_DIR . '/' . VIEW_DIR;?>/js/foundation.min.js"></script>
        <script>
            $(document).foundation();
        </script>
    </body>
</html>

<?php
// src/Views/_public/header.php

use bw_cart\Core\View;

?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>BW Cart - Produktwebsite mit einfachem Warenkorb</title>
    <link rel="stylesheet" href="<?php echo DOMAIN_DIR . '/' . VIEW_DIR;?>/css/style.css" />
    <script src="<?php echo DOMAIN_DIR . '/' . VIEW_DIR;?>/js/vendor/modernizr.js"></script>
</head>
<body>

    <div class="sticky">
        <nav class="top-bar" data-topbar role="navigation">
            <ul class="title-area">
                <li class="name">
                    <h1><a href="<?php echo DOMAIN_DIR;?>">BW Cart</a></h1>
                </li>
                <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
            </ul>

            <section class="top-bar-section">
                <!-- Left Nav Section -->
                <ul class="left">
                    <li><a href="<?php echo DOMAIN_DIR;?>">Homepage</a></li>
                </ul>

                <!-- Right Nav Section -->
                <ul class="right">
                    <li><a href="<?php echo DOMAIN_DIR;?>/cart/checkout/">Cart</a></li>
                </ul>
            </section>
        </nav>
    </div>




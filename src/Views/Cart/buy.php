<?php
// src/Views/Cart/buy.php

use bw_cart\Core\View;

View::useTemplate('_public/header.php', 'HeaderController');

?>

    <section class="row cart">
        <div class="large-12 cart--wrapper">
            <h1>Yeah!</h1>
            <p>Thank you for ordering all this great Stuff @BW Cart!</p>
            <p>Have a nice day!</p>
            <p class="right">
                <a href="<?php echo DOMAIN_DIR;?>/" class="button">Back to Homepage</a>
            </p>
        </div>
    </section>

<?php

View::useTemplate('_public/footer.php', 'FooterController');

?>
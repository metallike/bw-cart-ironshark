<?php
// src/Views/Index/index.php

use bw_cart\Core\View;

View::useTemplate('_public/header.php', 'HeaderController');

?>

<section class="row cart">
    <div class="large-12 cart--wrapper">
        <?php if (!$emptyCart) : ?>
        <table class="cart--table">
            <thead>
                <tr>
                    <td class="cart--table-name">Product</td>
                    <td class="cart--table-quantity">Quantity</td>
                    <td class="cart--table-edit"></td>
                    <td class="cart--table-price">Price</td>
                    <td class="cart--table-total">Total</td>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td class="cart--table-name"></td>
                    <td class="cart--table-quantity"></td>
                    <td class="cart--table-edit"></td>
                    <td class="cart--table-price"></td>
                    <td class="cart--table-total"><?php echo VIEW::currency($totalPrice);?></td>
                </tr>
            </tfoot>
            <tbody>
            <?php foreach($items as $item) { ?>
                <tr>
                    <td class="cart--table-name"><?php echo $item['name'];?></td>
                    <td class="cart--table-quantity"><?php echo $item['quantity'];?></td>
                    <td class="cart--table-edit"><a href="<?php echo DOMAIN_DIR;?>/cart/decreaseProduct/<?php echo $item['id'];?>/" class="cart--edit-button cart--button-decrease">-</a><a href="<?php echo DOMAIN_DIR;?>/cart/increaseProduct/<?php echo $item['id'];?>/" class="cart--edit-button cart--button-increase">+</a><a href="<?php echo DOMAIN_DIR;?>/cart/removeProduct/<?php echo $item['id'];?>/" class="cart--edit-button cart--button-delete">x</a></td>
                    <td class="cart--table-price"><?php echo VIEW::currency($item['price']);?></td>
                    <td class="cart--table-total"><?php echo VIEW::currency($item['totalPrice']);?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <p class="right">
            <a href="<?php echo DOMAIN_DIR;?>/cart/buy/" class="button">Order now!</a>
        </p>
        <?php else : ?>
        <p>You have no Products in your cart!</p>
        <?php endif; ?>
    </div>
</section>


<?php

View::useTemplate('_public/footer.php', 'FooterController');

?>

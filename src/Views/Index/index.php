<?php
// src/Views/Index/index.php

use bw_cart\Core\View;

View::useTemplate('_public/header.php', 'HeaderController');

?>

    <section class="row listing">
        <div class="large-12  listing--wrapper">
            <ul class="small-block-grid-2 medium-block-grid-4" itemscope itemtype="http://schema.org/ItemList">
                <?php foreach($items as $item) { ?>
                        <li class="listing--item-box" itemprop="itemListElement" itemscope itemtype="http://schema.org/Product">
                            <form action="./cart/addProduct/" method="post">
                                <input type="hidden" name="item-id" value="<?php echo $item['id'];?>">
                                <?php View::FormCSRF();?>
                                <a itemprop="url" href="#" class="listing--item-title">
                                    <span itemprop="name"><?php echo $item['name'];?></span>
                                </a>
                                <img alt="Produktbild von <?php echo $item['name'];?>" itemprop="image" src="<?php echo $item['image'];?>" class="listing--item-image" />
                                <span itemprop="description" class="listing--item-description"><?php echo $item['description'];?></span>
                                <span class="column listing--item-color">
                                    <span class="left">Color</span><span class="right" style="background-color: <?php echo $item['color'];?>;">&nbsp;</span>
                                </span>
                                <span class="column listing--item-price">
                                    <span class="left">Price</span><span class="right"><?php echo $item['price'];?></span>
                                </span>
                                <span class="column listing--item-quantity">
                                    <span class="left">Quantity</span><span class="right"><input type="number" placeholder="1" min="1" max="100" value="1" name="item-quantity"></span>
                                </span>
                                <input type="submit" class="button listing--item-in-cart" value="In den Warenkorb" name="item-add-cart">
                            </form>
                        </li>
                <?php } ?>
            </ul>
        </div>
    </section>
<?php

View::useTemplate('_public/footer.php', 'FooterController');

?>


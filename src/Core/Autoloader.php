<?php
// src/Core/Autoloader.php

namespace bw_cart\Core;

class Autoloader
{
    /**
     * @var string
     */
    private $namespace = '';

    /**
     * @var string
     */
    private $directory = '';

    /**
     * Autoloader constructor.
     *
     * @param null $namespace
     */
    public function __construct($namespace = null, $dir = '')
    {
        $this->namespace = $namespace;
        $this->directory = $dir;
    }

    public function register()
    {
        spl_autoload_register(array($this, 'autoloadClass'));
    }

    /**
     * @param $className
     *
     * @throws \Exception
     */
    public function autoloadClass($className)
    {
        if ($this->namespace !== null) {
            $className = str_replace($this->namespace . '\\', '', $className);
        }

        $className = str_replace('\\', '/', $className);

        $path = $this->directory . $className . '.php';

        if (file_exists($path)) {
            require_once $path;
        } else {
            throw new \Exception($path);
        }
    }
}
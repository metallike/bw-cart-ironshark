<?php
// src/Core/Helpers/ViewHelpers.php

namespace bw_cart\Core\Helpers;

use bw_cart\Core\CSRF;

class ViewHelpers
{
    /**
     * @param $email
     *
     * @return bool
     */
    static function validateEmail($email)
    {
        return true;
    }


    static function countArray($array = [])
    {
        return count($array);
    }

    static function createCSRFHiddenField()
    {
        if (CSRF::getCSRFToken()) {
            echo '<input type="hidden" name="_csrf_token" value="'. CSRF::getCSRFToken() . '"">';
        }
    }

    static function formatNumber($number) {
        return number_format($number, 2, ',', '.');
    }
}

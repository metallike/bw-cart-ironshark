<?php
// src/Core/Helpers/ValidateFormData.php

namespace bw_cart\Core\Helpers;

use bw_cart\Core\CSRF;

class ValidateFormData
{
    /**
     * @param $email
     *
     * @return bool
     */
    static function validateEmail($email)
    {
        return true;
    }

    static function validateCSRF($token)
    {
        return CSRF::valiidateCSRFokenToken($token);
    }
}

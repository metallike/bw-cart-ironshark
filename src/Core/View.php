<?php
// src/Core/View.php

namespace bw_cart\Core;

use bw_cart\Core\Helpers\ViewHelpers;

class View
{
    /**
     * @var string
     */
    protected $path = '';

    /**
     * @var string
     */
    protected $controller = '';

    /**
     * @var string
     */
    protected $action = '';

    /**
     * @var array
     */
    protected $vars = [];

    /**
     * @var null|string
     */
    protected $template = '';

    /**
     * View constructor.
     *
     * @param      $controllerName
     * @param      $actionName
     * @param null $template
     */
    public function __construct($controllerName, $actionName, $template = null)
    {
        $this->path       = VIEW_DIR;
        $this->controller = $controllerName;
        $this->action     = $actionName;

        if ($template !== null) {
            $this->template = $template;
        } else {
            $this->template = null;
        }
    }

    /**
     * @param array $vars
     */
    public function setVars($vars = [])
    {
        if (count($vars) != 0) {
            foreach ($vars as $key => $value) {
                $this->vars[$key] = $value;
            }
        }
    }

    /**
     * @throws \Exception
     */
    public function render()
    {
        if ($this->template !== null) {
            $template = $this->path . '/' . $this->template;
        } else {
            $template = $this->path . '/' . $this->controller . '/' . $this->action . '.php';
        }

        if (!file_exists($template)) {
            throw new \Exception($template);
        }

        foreach ($this->vars as $key => $value) {
            $$key = $value;
        }

        include $template;
    }

    /**
     * Static functions to use in Template
     */

    /**
     * @param        $template
     * @param string $controller
     *
     * @throws \Exception
     */
    static function useTemplate($template, $controller = 'IndexController')
    {
        $controller = '\\bw_cart\\Controllers\\' . $controller;
        $controller = new $controller();

        $view = new View($controller, 'index', $template);

        $controller->indexAction();

        $view->setVars($controller->getVars());
        $view->render();
    }

    /**
     *
     */
    static function FormCSRF()
    {
        return ViewHelpers::createCSRFHiddenField();
    }

    /**
     * @param array $array
     *
     * @return int
     */
    static function count($array = [])
    {
        return ViewHelpers::countArray($array);
    }

    static function currency($price)
    {
        return ViewHelpers::formatNumber($price) . ' &euro;';
    }




}
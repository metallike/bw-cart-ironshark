<?php
// src/Core/Router.php

namespace bw_cart\Core;

class Router
{
    /**
     * @var string
     */
    private $uri = '';

    /**
     * @var array
     */
    private $routeParts = [];

    /**
     * @var string
     */
    private $controllerName = '';

    /**
     * @var string
     */
    private $action = '';

    /**
     * @var string
     */
    private $params = '';

    /**
     * @var
     */
    public $controller;

    /**
     * @var
     */
    public $view;


    /**
     * Router constructor.
     *
     * @param null $uri
     */
    function __construct($uri = null)
    {
        if (isset($uri) && !empty($uri)) {
            $this->uri = $uri;
        } else {
            if (isset($_GET['_route']) && $_GET['_route']) {
                $this->uri = $_GET['_route'];
            } else {
                $this->uri = null;
            }
        }

        $this->routeParts = $this->explodeRouteParts($this->uri);
        $this->controllerName = $this->setController();
        $this->action = $this->setAction();
        $this->params = $this->setParams();

        $this->fireController();
    }

    /**
     *
     */
    private function fireController()
    {
        $controllerClass = '\\bw_cart\\Controllers\\' . $this->controllerName . 'Controller';

        $this->controller = new $controllerClass();
        $this->view = new View($this->controllerName, $this->action);
    }

    /**
     * @return mixed
     */
    private function fireAction()
    {
        $action = $this->action . 'Action';

        if ($this->params !== null) {
            return $this->controller->$action($this->params);
        } else {
            return $this->controller->$action();
        }
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        $this->fireAction();
        $this->view->setVars($this->controller->getVars());
        return $this->view->render();
    }

    /**
     * @param $uri
     *
     * @return array
     */
    private function explodeRouteParts($uri)
    {
        if ($uri === null) {
            $routeParts = [];
        } else {
            $explodeRoute = explode('/', $uri);

            $countRoutes = count($explodeRoute);

            if  ($countRoutes === 1 || $countRoutes === 2 && empty($explodeRoute[1])) {
                $routeParts = [
                    'controller'    =>  $explodeRoute[0]
                ];
            }

            if ($countRoutes === 2 && !empty($explodeRoute[1]) || $countRoutes >= 3 && empty($explodeRoute[2])) {
                $routeParts = [
                    'controller' => $explodeRoute[0],
                    'action' => $explodeRoute[1]
                ];
            }

            if ($countRoutes >= 3 && !empty($explodeRoute[2])) {
                $routeParts = [
                    'controller'    =>  $explodeRoute[0],
                    'action'        =>  $explodeRoute[1],
                    'params'        =>  $explodeRoute[2]
                ];
            }
        }

        return $routeParts;
    }

    /**
     * @return string
     */
    private function setController()
    {
        if (!isset($this->routeParts['controller'])) {
            $controller = 'index';
        } else {
            $controller = $this->routeParts['controller'];
        }

        return ucfirst($controller);
    }

    /**
     * @return string
     */
    private function setAction()
    {
        if (!isset($this->routeParts['action'])) {
            $action = 'index';
        } else {
            $action = $this->routeParts['action'];
        }

        return $action;
    }

    /**
     * @return mixed
     */
    private function setParams()
    {
        if (isset($this->routeParts['params'])) {
            $param = $this->routeParts['params'];
        } else {
            $param = null;
        }

        return $param;
    }

    /**
     * @return null|string
     */
    public function getRoute()
    {
        return $this->uri;
    }

    /**
     * @return array
     */
    public function getRouteParts()
    {
        return $this->routeParts;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controllerName;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    static function routeBack($controller) {
        header('Location: '. DOMAIN_DIR . '/' . $controller);
    }
}

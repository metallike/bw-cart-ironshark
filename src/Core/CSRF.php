<?php
// src/Core/CSRF.php

namespace bw_cart\Core;

class CSRF
{
    /**
     *
     */
    static function createCSRFToken()
    {
        if (!isset($_SESSION['bw_cart_csrf_token'])) {
            $token = md5(rand(1, 1000));
            $_SESSION['bw_cart_csrf_token'] = $token;
        }
    }

    /**
     * @return bool
     */
    static function getCSRFToken()
    {
        if (isset($_SESSION['bw_cart_csrf_token'])) {
            return $_SESSION['bw_cart_csrf_token'];
        } else {
            return false;
        }
    }

    /**
     * @param $token
     *
     * @return bool
     * @throws \Exception
     */
    static function valiidateCSRFokenToken($token)
    {
        if ($token === $_SESSION['bw_cart_csrf_token']) {
            return true;
        } else {
            throw new \Exception('No valid CSRF Token! Reload and try again!');
        }
    }
}
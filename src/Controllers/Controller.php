<?php
// src/Controllers/Controller.php

namespace bw_cart\Controllers;

use Doctrine\ORM\EntityManager;

class Controller
{
    /**
     * @var array
     */
    public $vars = [];

    /**
     * @param      $var
     * @param null $value
     *
     * @throws \Exception
     */
    public function setVar($var, $value = null)
    {
        if (!is_array($var) && isset($var) && $value !== null) {
            if (strtolower($var) === 'debug') {
                throw new \Exception('Var "debug" already exists!');
            } else {
                $this->vars[$var] = $value;
            }
        }
    }

    /**
     * @return array
     */
    public function getVars()
    {
        $this->vars['debug'] = $this->vars;

        return $this->vars;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     * @throws \Doctrine\ORM\ORMException
     */
    protected function getEntityManager()
    {
        global $GLOBALS;
        return EntityManager::create($GLOBALS['_doctrine_connection'], $GLOBALS['_doctrine_config']);
    }
}

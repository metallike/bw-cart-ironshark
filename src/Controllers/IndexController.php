<?php
// src/Controllers/IndexController.php

namespace bw_cart\Controllers;

use Doctrine\ORM\EntityManager;

class IndexController extends Controller
{
    public function indexAction()
    {
        $entityManager = $this->getEntityManager();
        $articleRepository = $entityManager->getRepository('\bw_cart\Models\Articles');

        $articles = $articleRepository->findAll();

        foreach ($articles as $article) {
            //echo sprintf("-%s\n", $article->getName());
            $items[$article->getID()] = [
                'id'            =>  $article->getID(),
                'name'          =>  $article->getName(),
                'description'   =>  $article->getDescription(),
                'image'         =>  $article->getImage(),
                'price'         =>  $article->getPrice(),
                'color'         =>  $article->getColor()
            ];
        }

        $this->setVar('items', $items);


    }

    public function orderAction()
    {

    }
}
<?php
// src/Controllers/HeaderController.php

namespace bw_cart\Controllers;

class HeaderController extends Controller
{
    public function indexAction()
    {
        $this->setVar('cartItems', ['hose', 'socken', 'jacke', 'nicki']);
    }
}
<?php
// src/Controllers/CartController.php

namespace bw_cart\Controllers;

use bw_cart\Core\Helpers\ValidateFormData;
use Doctrine\ORM\EntityManager;
use bw_cart\Core\Router;

class CartController extends Controller
{
    public function addProductAction()
    {
        if ($_POST['item-add-cart']) {
            if (!ValidateFormData::validateCSRF($_POST['_csrf_token']) || $_POST['item-quantity'] <= 0) {
                return false;
            } else {

                if (array_key_exists($_POST['item-id'], $_SESSION['bw_cart_in_cart'])) {
                    $_SESSION['bw_cart_in_cart'][$_POST['item-id']] = [
                        'id'        =>  $_POST['item-id'],
                        'quantity'  =>  $_SESSION['bw_cart_in_cart'][$_POST['item-id']]['quantity'] + $_POST['item-quantity']
                    ];
                } else {
                    $_SESSION['bw_cart_in_cart'][$_POST['item-id']] = [
                        'id'        =>  $_POST['item-id'],
                        'quantity'  =>  $_POST['item-quantity']
                    ];
                }
            }
        } else {
            return false;
        }

        Router::routeBack('cart/checkout');
    }

    public function checkoutAction()
    {
        $entityManager = $this->getEntityManager();

        if (isset($_SESSION['bw_cart_in_cart']) && count($_SESSION['bw_cart_in_cart']) >= 1)
        {
            $items = [];
            $totalPrice = 0;
            foreach($_SESSION['bw_cart_in_cart'] as $item) {
                $itemData = $entityManager->find('\bw_cart\Models\Articles', $item['id']);

                if ($itemData !== null) {
                    $items[$item['id']]['id'] = $itemData->getId();
                    $items[$item['id']]['name'] = $itemData->getName();
                    $items[$item['id']]['price'] = $itemData->getPrice();
                    $items[$item['id']]['quantity'] = $item['quantity'];
                    $items[$item['id']]['totalPrice'] = $item['quantity']*$items[$item['id']]['price'];
                    $totalPrice = $totalPrice + $items[$item['id']]['totalPrice'];
                }
            }

            $this->setVar('items', $items);
            $this->setVar('totalPrice', $totalPrice);
        } else {
            $this->setVar('emptyCart', true);
        }
    }

    public function increaseProductAction($id)
    {
        if (isset($_SESSION['bw_cart_in_cart'][$id])) {
            $_SESSION['bw_cart_in_cart'][$id] = [
                'id'        =>  $id,
                'quantity'  =>  $_SESSION['bw_cart_in_cart'][$id]['quantity'] + 1
            ];
        }

        Router::routeBack('cart/checkout');

    }

    public function decreaseProductAction($id)
    {
        if (isset($_SESSION['bw_cart_in_cart'][$id]) && $_SESSION['bw_cart_in_cart'][$id]['quantity'] > 1) {
            $_SESSION['bw_cart_in_cart'][$id] = [
                'id'        =>  $id,
                'quantity'  =>  $_SESSION['bw_cart_in_cart'][$id]['quantity'] - 1
            ];
        } else {
            if ($_SESSION['bw_cart_in_cart'][$id]['quantity'] <= 1) {
                $this->removeProductAction($id);
            }
        }

        Router::routeBack('cart/checkout');

    }

    public function removeProductAction($id)
    {
        if (isset($_SESSION['bw_cart_in_cart'][$id])) {
            unset($_SESSION['bw_cart_in_cart'][$id]);
        }

        Router::routeBack('cart/checkout');
    }

    public function buyAction()
    {
        unset($_SESSION['bw_cart_in_cart']);
    }
}

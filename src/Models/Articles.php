<?php
// src/Models/Articles.php

namespace bw_cart\Models;

/**
 * @Entity @Table(name="cart_articles")
 */
class Articles
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string")
     */
    protected $name;

    /**
     * @Column(type="text")
     */
    protected $description;

    /**
     * @Column(type="text")
     */
    protected $image;

    /**
     * @Column(type="decimal", scale=2)
     */
    protected $price;

    /**
     * @Column(type="string")
     */
    protected $color;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ordernumber
     *
     * @param string $ordernumber
     * @return Articles
     */
    public function setOrdernumber($ordernumber)
    {
        $this->ordernumber = $ordernumber;

        return $this;
    }

    /**
     * Get ordernumber
     *
     * @return string 
     */
    public function getOrdernumber()
    {
        return $this->ordernumber;
    }

    /**
     * Set mainnumber
     *
     * @param string $mainnumber
     * @return Articles
     */
    public function setMainnumber($mainnumber)
    {
        $this->mainnumber = $mainnumber;

        return $this;
    }

    /**
     * Get mainnumber
     *
     * @return string 
     */
    public function getMainnumber()
    {
        return $this->mainnumber;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Articles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Articles
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Articles
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Articles
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Articles
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }
}

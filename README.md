# BW Cart | IronShark #

Ich hoffe, ich habe alles beachtet, was in der PDF stand. Ich war mir nicht sicher, ob ich den Checkout-Prozess komplett abbilden soll oder direkt vom Warenkorb aus bestellt wird. Ich habe die kürzere Variante gewählt ;) Auf Bilder und hübsch bunte Formatierung habe ich aus Zeitmangel verzichtet. Ich denke aber man sieht, was es mal werden sollte.

## Installation ##
Bitte Datenbank entsprechend anlegen (siehe cart.sql) und Angaben in der config/config.php anpassen.